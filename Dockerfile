FROM registry.access.redhat.com/ubi8/nginx-118

ENV NGINX_VERSION=1.18

COPY ./nginx.conf "${NGINX_CONF_PATH}"
COPY es6/ ./

# Run script uses standard ways to run the application
CMD nginx -g "daemon off;"
