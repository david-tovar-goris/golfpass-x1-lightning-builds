#!/usr/bin/env node

/* eslint-env node */

// Based on the answer
// https://github.com/conventional-changelog/standard-version/issues/434#issuecomment-547961009

const { writeJson } = require("fs-extra");
const { execSync } = require("child_process");
const pkg = require("./package.json");
const lock = require("./package-lock.json");
const metadata = require("./metadata.json");

const tag = execSync(`git describe --abbrev=0`, { encoding: "utf8" }).trim();
const version = tag.slice(1);

const updatePackageJson = async () => {
  pkg.version = version;

  await writeJson("./package.json", pkg, { spaces: 2 });

  console.log(`package.json updated to version: ${version}`);
};

const updatePackageLockJson = async () => {
  lock.version = version;
  lock.packages[""].version = version;

  await writeJson("./package-lock.json", lock, { spaces: 2 });

  console.log(`package-lock.json updated to version: ${version}`);
};

const updateMetadataJson = async () => {
  metadata.version = version;

  await writeJson("./metadata.json", metadata, { spaces: 2 });

  console.log(`metadata.json updated to version: ${version}`);
};

(async () => {
  await Promise.all([
    updatePackageJson(),
    updatePackageLockJson(),
    updateMetadataJson(),
  ]);

  execSync(`git add ./*.json`);
  execSync(`git commit -m "chore: bump version in files [skip ci]"`);
  execSync("git push");

  console.log("Changes committed and pushed.");
})();
